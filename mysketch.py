#!/usr/bin/env python3
import IsoSpecPy as iso
import prasopes.graphtools as gt
import prasopes.datatools as dt
import re
import argparse
from timsconvert.parse import *
from timsconvert.classes import *
from matplotlib.backends.backend_qt5agg import\
        FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import docx
import pandas as pd
from docx.shared import *
from io import BytesIO


def binit(x, y, minstep, length):
    # Parts stolen from prasopes datasets bruker thingiez:
    sortx = np.sort(x)
    stepsx = sortx[1:] - sortx[:-1]
    binspos = np.where(stepsx > minstep)[0]
    bins = sortx[:-1][binspos] + (stepsx[binspos]/2)
    binpos = np.digitize(x, bins)
    bindx = np.bincount(binpos, x) / np.bincount(binpos)
    bindy = np.bincount(binpos, y) / length
    return bindx, bindy

def gauss(x, xz, sig):
    #resolving power - 10percent valley
    #sig = ((R/xz)*np.sqrt(-8*np.log(1/20)))**-1
    #dirty speedup hack
    if abs(x-xz) > 0.5:
        return 0
    y = np.exp(-0.5*((x-xz)/sig)**2)
    return y

def lorentz(x, xz, R):
    #resolving power - 10percent valley
    sig = (np.sqrt(76)*(R/xz))**-1
    #resolving power - fwhm style
    sig = 0.5*(xz/R)
    k = np.pi*sig
    y = k*((np.pi*sig*(1+((x-xz)/sig)**2))**-1)
    return y


def getymax(x, y, xmin, xmax):
    return np.max(y[dt.argsubselect(x, xmin, xmax)])

def getxymax(x, y, xmin, xmax):
    arg = np.where(y[dt.argsubselect(
        x, xmin, xmax)] == getymax(x, y, xmin, xmax))[0]
    xmax = x[dt.argsubselect(x, xmin, xmax)][arg][0]
    return xmax

def get_predict(formula, charge):
    parsed = re.findall(r"([A-Z][a-z]*)(\d*)|(\()|(\))(\d*)", formula)
    structure_dict = {}
    for element_details in parsed:
        element = element_details[0]
        if element not in structure_dict:
            structure_dict[element] = 0
        element_count = sum(
            [int(x) for x in element_details[1:] if x != ""])
        if element_count > 0:
            structure_dict[element] += element_count
        else:
            structure_dict[element] += 1
    mE = 0.000548579909065
    predict = iso.IsoTotalProb(formula=structure_dict, prob_to_cover=.995)
    x = (predict.np_masses() - (charge * mE)) / abs(charge)
    y = predict.np_probs() / max(predict.np_probs())
    return x, y


def get_simul(predict, resolution, xrange, ymax):
    simyg = np.zeros(len(xrange))
    sqrtm8nplog0p5 = np.sqrt(-8*np.log(1/2))
    for mz, height in zip(*predict):
        #resolving power - fwhm style (100percent valley?)
        sig = ((resolution/mz)*sqrtm8nplog0p5)**-1
        y = np.asarray([gauss(i, mz, sig) for i in xrange]) * height
        simyg = simyg + y
    simyg = simyg * ymax
    return simyg

def generate_report(inf, outf, samplename, expmasses, baf2sql_dll):
    ms_bins = 0.001
    mint = 15
    maxt = 27
    xinch, yinch, dpi = 10.5, 3, 400
    lowend, highend = -2, 7
    resolution = 60000

    #load_data
    data = baf_data(inf, baf2sql_dll)
    frames = data.frames[(data.frames['Rt'] > mint) &
                         (data.frames['Rt'] < maxt)].to_dict(orient='records')
    masses, intensities = [], []
    for i in frames:
        mz, ints = extract_baf_spectrum(
                data, frame_dict=i, mode="profile",
                profile_bins=0, encoding=64)
        masses.append(mz)
        intensities.append(ints)
    masses = np.concatenate(masses)
    intensities = np.concatenate(intensities)
    mz, ints = binit(masses, intensities, ms_bins, len(frames))


    #prepare_document
    doc = docx.Document()
    section = doc.sections[0]
    section.page_width = Cm(21)
    section.page_height = Cm(29.7)
    section.left_margin = Inches(0.5)
    section.right_margin = Inches(0.5)
    section.top_margin = Inches(0.5)
    section.bottom_margin = Inches(0.5)
    style = doc.styles['Normal']
    style.paragraph_format.space_before = Cm(0)
    style.paragraph_format.space_after = Cm(0)
    font = style.font
    font.name = 'Arial'
    font.size = Pt(11)

    #populate document
    #figure_description
    fig = Figure(figsize=(xinch, yinch), dpi=dpi, constrained_layout=True)
    overal_plot = fig.add_subplot()
    plot_data = {"name": "Full spectrum",
                 "xlabel": "$m/z$ →", "ylabel": "$Intensity$ $(arb)$ →",
                 "annotation": [], "texts": [],
                 "ancx": 12, "ancy": 8, "xtics": 15}
    gt.pop_plot(mz, ints, overal_plot, plot_data)
    overal_plot.set_xlim(0, 2500)
    gt.ann_spec(overal_plot, plot_data, ann_limit=0.1)
    cache_file = BytesIO()
    fig.savefig(cache_file)
    heading = doc.add_heading(f'HRMS REPORT - sample {samplename}')
    doc.add_paragraph("used IsoSpec - https://matteolacki.github.io/IsoSpec/ "
                      "for isotopic distribution (citations in the link)\n")
    heading.alignment = docx.enum.text.WD_ALIGN_PARAGRAPH.CENTER
    doc.add_paragraph("Sample spectrum:")
    picture = doc.add_picture(cache_file)
    picture.width = docx.shared.Inches(7)
    picture.height = docx.shared.Inches(2)
    for i in list(range(len(expmasses))):
        doc.add_paragraph(f"\n\nScan for {expmasses[i]}:")
        fig = Figure(figsize=(xinch, yinch), dpi=dpi, constrained_layout=True)
        c=1+i
        formula=expmasses[i]
        print("processing formula: ", formula)
        pluses = re.search(r'\++',formula)
        charge = len(pluses[0]) if pluses else 1
        minuses = pluses = re.search(r'\-+',formula)
        if minuses:
            charge = -len(minuses[0])
        predict = get_predict(formula, charge)
        MHmass = int(predict[0][0])
        real1_plot = fig.add_subplot(121)
        real1_data = {"name": "$Experimental$ $spectrum$ $detail$",
                      "xlabel": "$m/z$ →", "ylabel": "$Intensity$ $(arb)$ →",
                      "annotation": [], "texts": [],
                      "ancx": 10, "ancy": 4, "xtics": 8}
        gt.pop_plot(mz, ints, real1_plot, real1_data)
        real1_plot.set_xlim(MHmass+lowend, MHmass+highend)
        gt.autozoomy(real1_plot)
        gt.ann_spec(real1_plot, real1_data, ann_limit=0.01)

        simx = mz[(mz > MHmass+lowend-1) & (mz < MHmass+highend+1)]
        simx = np.arange(simx[0],simx[-1],np.average(np.diff(simx))/50)
        sim1_plot = fig.add_subplot(122)
        formtext = re.sub(r'([0-9]+)',r'$_{\1}$',formula).strip("+")
        if charge == 1:
            formtext += "$^+$"
        elif charge == -1:
            formtext += "$^-$"
        elif charge < -1:
            formtext += f"$^{{{charge}-}}$"
        else:
            formtext += f"$^{{{charge}+}}$"

        sim1_data = {"name": f"{formtext} $Simulated$ $spectrum$",
                     "xlabel": "$m/z$ →", "ylabel": "$Intensity$ $(arb)$ →",
                     "annotation": [], "texts": [],
                     "ancx": 10, "ancy": 10, "xtics": 8}
        sim1_plot.set_xlim(MHmass+lowend, MHmass+highend)
        ymax = getymax(mz, ints, *sim1_plot.get_xlim())
        simy = get_simul(predict, resolution, simx, ymax)
        gt.pop_plot(simx, simy, sim1_plot, sim1_data)
        gt.autozoomy(sim1_plot)
        gt.ann_spec(sim1_plot, sim1_data, ann_limit=0.01)
        cache_file = BytesIO()
        fig.savefig(cache_file)
        picture = doc.add_picture(cache_file)
        picture.width = docx.shared.Inches(7)
        picture.height = docx.shared.Inches(2)
        for i,j in zip(*predict):
            if j > 0.05:
                xreal = getxymax(mz, ints, i-0.05, i+0.05)
                diff = abs(xreal-i)*1000
                diffppm = (diff*1000)/i
                doc.add_paragraph(f'calculated: {i:.4f}, found: {xreal:.4f},'
                                  f'diff={diff:.1f} mmu ({diffppm:.1f} ppm)')
    doc.save(outf)


if __name__ == "__main__":
    #loading the relevant data
    clinput = argparse.ArgumentParser()
    clinput.add_argument('-i', '--input', required=True, help='Specify folder'
                         'where batch.csv measurements description file and'
                         'measurements .d folders are located.', type=str)
    args = clinput.parse_args()

    measurements = [i for i in os.listdir(args.input) if (
        i.endswith(".d") and os.path.isdir(i))]
    if not "batch.csv" in os.listdir(args.input) or os.path.isdir("batch.csv"):
        print("batch.csv file not found, nothing to do, exiting now...")
        quit()

    #processing
    intable = pd.read_csv("batch.csv").values.tolist()
    baf2sql_dll = init_baf2sql_dll(BAF2SQL_DLL_FILE_NAME)
    for i in intable:
        print(f'Processing {i[0]}')
        files = [f for f in measurements if f.upper().startswith(i[0].upper())]
        print(f'found {files}')
        print(f'Using {files[0]} for the analysis')
        print(f'Will search for {i[-1]}.')
        outf=f'{i[0]}_hrms_report.docx'
        print(f'Will write the report to {outf}')
        generate_report(files[0], outf, i[0], i[-1].split(","), baf2sql_dll)

    print("Bye")
    quit()

